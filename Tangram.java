
import com.sun.opengl.util.Animator;
import java.awt.Frame;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.media.opengl.GL;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLCanvas;
import javax.media.opengl.GLCapabilities;
import javax.media.opengl.GLEventListener;
import javax.media.opengl.glu.GLU;
import javax.swing.JFrame;
import static javax.swing.JFrame.EXIT_ON_CLOSE;

public class Tangram extends JFrame {
    
    
    static GL gl;
    static GLU glu;
    
    public Tangram (){
    setTitle("Practica Lineas");
    setSize(600,600);
    
    //instanciar la clase graphics
    Tangram.GraphicListener listener = new Tangram.GraphicListener();
    
    //Crear el canvas
    GLCanvas canvas = new GLCanvas(new GLCapabilities());
    canvas.addGLEventListener(listener);
    getContentPane().add(canvas);
    }
    
    public static void main (String args[]){
        Tangram frame = new Tangram();
        frame.setVisible(true);
        frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
    }
    
 public class GraphicListener implements GLEventListener {

        @Override
        public void init(GLAutoDrawable drawable) { 
            gl = drawable.getGL();
            gl.glEnable(gl.GL_BLEND);
            gl.glBlendFunc( gl.GL_SRC_ALPHA, gl.GL_ONE_MINUS_SRC_ALPHA);  
        }

        @Override
        public void display(GLAutoDrawable drawable) {
            gl.glClearColor(0.412f, 0.423f, 0.987f, 0.4f);             //Establecemos los parametros de proyeccion    
            gl.glLoadIdentity(); 
            gl.glOrtho(0.0, 1.0, 0.0, 1.0, -1.0, 1.0); 
            
            gl.glPolygonMode(GL.GL_FRONT, GL.GL_POINTS);
            gl.glColor3f(0.876f,0.101f,567f);
            Triangulo(gl,   1 ,   1 ,   1 ,  0.5f ,0.8f ,0.8f ); 
            
            gl.glColor3f(0.453446f ,0.7f ,0.3110f);
            Triangulo(gl, 1.0f , 0.5f , 0.86f, 0.23f, 1.0f , 0.19f );
            
            gl.glColor3f(0.92f,0.10f,0.53f);
            Lados4(gl, 1.0f , 0.5f, 0.8f, 0.8f, 0.6f, 0.3f, 0.88f, 0.22f  );
            
            gl.glColor3f(0.10f,1,0.34f);
            Lados4(gl, 1.0f , 0.19f , 0.6f , 0.3f , 0.6f , 0.0f , 1.0f , 0.0f );
               //      AD      AD     AI     AI     ABI     ABI     ABD     ABD
               
            gl.glColor3f(0.877f,0.0811f,0.899f);
            Triangulo(gl, .5f, 1.0f , .8f , 1.0f , .683f , .5f );
            
            gl.glColor3f(.9f,.9f,.9f);
            //Lados4(gl, .3f, 1.0f , .5f , 1.0f , .6f , 0.0f, .3f, 0.0f );
            
            gl.glColor3f(0.003f,0.01f,1);
            Triangulo(gl, .6f,0.0f , .6f, .3f, .57f, .3f  );
            
            gl.glColor3f(.255f,255f,255f);
            Lados4(gl,.5f , 1.0f, .683f , .5f , .6f , .3f , .57f, .3f  );
            
            gl.glColor3f(.690f,.210f,.90f);
            Triangulo(gl,0.0f ,0.0f, 0.0f, .5f, .5f, .0f );
            
            gl.glColor3f(1,0,1);
            Triangulo(gl, .0f, .5f, .5f, 1.0f, .0f, 1.0f);   //
            
            gl.glColor3f(0,0,1);
            //Lados4(gl, );
            
            gl.glColor3f(0.982f, 0.232f, 0.900f);
            Triangulo(gl, .8f , 1.0f, .683f,.5f, .8f,.8f );
            
            gl.glColor3f(0.3f,0.101f,.001f);
            Triangulo(gl, 0.8f , 0.8f , 0.8f , 1.0f , 1.0f , 1.0f );
            gl.glEnd();
            gl.glFlush();
            
            
            
            
            //Establece la cara que se desea pintar y la forma en la que se desea pintar 
        /*    dibujaPoligono(gl, 0.0f, 0.0f, 0.5f, 0.5f, 0.0f, 1f); 
             
            gl.glPolygonMode(GL.GL_FRONT, GL.GL_LINES); 
            gl.glColor3f(1.0f, 1.0f, 0.0f);//rojo 
            dibujaPoligono(gl, 0.0f, 1.0f, 0.5f, 0.5f, 1f, 1f); 
             
            gl.glPolygonMode(GL.GL_FRONT, GL.GL_FILL); 
            gl.glColor3f(0.2f, 0.8f, 0.1f);//rojo 
            dibujaPoligono(gl, 0.0f, 0.0f, 0.5f, 0.0f, 0.25f, 0.25f); 
             
            gl.glPolygonMode(GL.GL_FRONT, GL.GL_LINES);             gl.glColor3f(0.974f, 0.003f, 1.0f);//rojo 
            dibujaPoligono(gl, 0.5f, 0.0f, 1.0f, 0.0f, 1.0f, 0.5f); 
             
            gl.glPolygonMode(GL.GL_FRONT, GL.GL_POINTS); 
            gl.glColor3f(0.0f, 0.0f, 0.7f);//rojo 
            dibujaPoligono(gl, 0.5f, 0.5f, 0.75f, 0.25f, 0.75f, 0.75f); 
             
            gl.glPolygonMode(GL.GL_FRONT, GL.GL_FILL); 
            gl.glColor3f(255f, 0.0f, 0.0f); 
            dibujaQuad(gl, 0.25f, 0.25f, 0.5f, 0.0f, 0.75f, 0.25f,0.5f,0.5f); 
             
            gl.glPolygonMode(GL.GL_FRONT, GL.GL_POINTS); 
            gl.glColor3f(0.0f, 1.0f, 0.0f); 
            dibujaQuad(gl, 0.75f, 0.25f, 1.0f, 0.5f, 1.0f, 1.0f,0.75f,0.75f); */
             
            // Procesa todas las subrutinas de JOGL, tan rapido como sea posible             gl.glFlush(); 


      }

        
         
        public void Triangulo(GL gl, float x1, float y1, float x2, float y2, float x3, float y3) { 
            gl.glBegin(GL.GL_POLYGON);            
            gl.glVertex2f(x1, y1);             
            gl.glVertex2f(x2, y2);             
            gl.glVertex2f(x3, y3); 
            gl.glEnd(); 
        } 
 
        public void Lados4(GL gl, float x1, float y1, float x2, float y2, float x3, float y3, float x4, float y4) {             
            gl.glBegin(GL.GL_QUADS);             
            gl.glVertex2f(x1, y1);             
            gl.glVertex2f(x2, y2);             
            gl.glVertex2f(x3, y3);             
            gl.glVertex2f(x4, y4); 
            gl.glEnd(); 
        } 


        @Override
        public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {
        }

        @Override
        public void displayChanged(GLAutoDrawable drawable, boolean modeChanged, boolean deviceChanged) {
        }
     
 }
}